
"""
Helper Components / Functions
"""

def reverse_mapping(mapping: dict) -> dict:
    """
    Creates the inverse mapping of a dict type
    """
    return {v: k for k, v in mapping.items()}

