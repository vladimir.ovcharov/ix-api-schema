
from django.conf import settings
from django.core import exceptions

# Export rest framework components
from rest_framework.serializers import *

# Test if django was set up correctly
DJANGO_CONTEXT = True
try:
    settings.VAR
except exceptions.ImproperlyConfigured:
    DJANGO_CONTEXT = False
except AttributeError:
    pass # We are testing for a non existing variable


if DJANGO_CONTEXT:
    from ixapi_schema.openapi.components.django import *
else:
    from ixapi_schema.openapi.components.shim import *


from ixapi_schema.openapi.components.filters import *

