
import enum

from django.utils import timezone

from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import crm
from ixapi_schema.v2.constants import ipam



class IpVersionField(components.IntegerField):
    def to_representation(self, value):
        """
        Serialize ip version.

        :param value: An IpVersion enum
        """
        return value.value


    def to_internal_value(self, data):
        """
        Deserialize ip version

        :param data: IP version integer
        """
        return ipam.IpVersion(data)


class MacAddressField(components.CharField):
    """
    Derived from a char field, we validate that the input
    is a mac address.
    """
    def to_internal_value(self, data):
        """Parse and validate mac address"""
        if not isinstance(data, str):
            raise components.ValidationError(
                "A MAC address input must be a string")

        if not (len(data) > 10 and len(data) <= 17):
            raise components.ValidationError(
                "Invalid MAC address length")

        tokens = data.split(":")
        if not len(tokens) == 6:
            raise components.ValidationError(
                "MAC address too short")

        try:
            [int(t, 16) for t in tokens]
        except ValueError:
            raise components.ValidationError(
                "A mac address must only contain hexadecimal byte values")

        # Normalize
        return data.lower()


class IpAddressBase(
        crm.Ownable,
        components.Component,
    ):
    """IP-Address"""
    version = IpVersionField(
        help_text="""
            The version of the internet protocol.

            Example: 4
        """)
    address = components.CharField(
        help_text="""
                IPv4 or IPv6 Address in the following format:
                - IPv4: [dot-decimal notation](https://en.wikipedia.org/wiki/Dot-decimal_notation)
                - IPv6: hexadecimal colon separated notation

                Example: "23.142.52.0"
            """)
    prefix_length = components.IntegerField(
        min_value=0,
        max_value=128,
        help_text="""
            The CIDR ip prefix length

            Example: 23
        """)

    fqdn = components.CharField(
        max_length=100,
        required=False,
        allow_null=True,
        allow_blank=False)

    valid_not_before = components.DateTimeField(
        default=timezone.now,
        allow_null=True)
    valid_not_after = components.DateTimeField(
        default=None,
        allow_null=True)


class IpAddressRequest(IpAddressBase):
    """IP-Address / Prefix allocation Request"""


class IpAddressUpdate(IpAddressBase):
    """IP-Address Update"""

class IpAddress(IpAddressBase):
    """IP-Address"""
    id = components.CharField(max_length=80)


class MacAddressBase(
        crm.Ownable,
        components.Component,
    ):
    """MAC-Address""" # And cheese address
    address = MacAddressField(
        help_text="""
            MAC Address Value, formatted hexadecimal values with colons.

            Example: 42:23:bc:8e:b8:b0
        """)

    valid_not_before = components.DateTimeField(
        allow_null=True,
        default=timezone.now)
    valid_not_after = components.DateTimeField(
        default=None,
        allow_null=True)


class MacAddressRequest(MacAddressBase):
    """MAC-Address Request"""

class MacAddress(MacAddressBase):
    """MAC-Address"""
    id = components.CharField(max_length=80)

