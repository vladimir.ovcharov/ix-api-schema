

from ixapi_schema.coupling import qs
from ixapi_schema.openapi import components
from ixapi_schema.v2.entities import events


class Manageable(components.Component):
    """Managed"""
    managing_account = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Account"),
        help_text="""
            The `id` of the account responsible for managing the service via
            the API.
            Used to be `billing_customer`.

            Example: "238189294"
        """)


class ExternalRef(components.Component):
    """External Reference"""
    external_ref = components.CharField(
        default=None,
        allow_null=True,
        allow_blank=False,
        max_length=128,
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)


class Consumable(components.Component):
    """Consumable"""
    consuming_account = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Account"),
        help_text="""
            The `id` of the account consuming a service.

            Used to be `owning_customer`.

            Example: "2381982"
        """)

# Composit shortcut
class Ownable(
        Manageable,
        Consumable,
        ExternalRef,
        components.Component,
    ):
    """Ownable"""


class Contactable(components.Component):
    """Contactable"""
    contacts = components.PrimaryKeyRelatedField(
        required=True,
        queryset=qs.Any("crm.Contact"),
        many=True,
        help_text="""
            A set of contacts. See the documentation
            on the specific `required_contact_roles` on what
            contacts to provide.

            Example: ["c-impl:123", "c-noc:331"]
        """)


class Invoiceable(components.Component):
    """Invoiceable"""
    purchase_order = components.CharField(
        allow_blank=True,
        allow_null=False,
        default="",
        max_length=80,
        help_text="""
            Purchase Order ID which will be displayed on the invoice.

            Example: "Project: DC Moon"
        """)
    contract_ref = components.CharField(
        allow_null=True,
        allow_blank=False,
        default=None,
        max_length=128,
        help_text="""
            A reference to a contract.

            Example: "contract:31824"
        """)

    billing_account = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Account"),
        help_text="""
            Please note, that an account requires billing_information
            to be filled in.
        """)


class Address(components.Component):

    country = components.CharField(
        max_length=2,
        help_text="""
            ISO 3166-1 alpha-2 country code, for example DE
            example: US
        """)
    locality = components.CharField(
        max_length=40,
        help_text="""
            The locality/city. For example, Mountain View.
            example: Mountain View
        """)
    region = components.CharField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The region. For example, CA
            example: CA
        """)
    postal_code = components.CharField(
        max_length=24,
        help_text="""
            A postal code. For example, 9404
            example: "9409"
        """)
    street_address = components.CharField(
        max_length=80,
        help_text="""
            The street address. For example, 1600 Amphitheatre Pkwy.
            example: 1600 Amphitheatre Pkwy.
        """)
    post_office_box_number = components.CharField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The post office box number for PO box addresses.
            example: "2335232"
        """)


#
# Account Serializers
#

class BillingInformation(components.Component):
    name = components.CharField(
        max_length=80,
        help_text="""
            Name of the organisation receiving invoices.

            Example: Moonoc Network Services LLS.
        """)

    address = Address()

    vat_number = components.CharField(
        min_length=2,
        max_length=20,
        required=False,
        allow_null=True,
        help_text="""
            Value-added tax number, required for
            european reverse charge system.

            Example: UK2300000042
        """)


class AccountBase(components.Component):
    """Account Base"""
    managing_account = components.PrimaryKeyRelatedField(
        default=None,
        allow_null=True,
        queryset=qs.Any("crm.Account"),
        help_text="""
            The `id` of a managing account. Can be used for creating
            a customer hierachy.

            Example: IX:Account:231
        """)

    name = components.CharField(
        max_length=80, required=True,
        help_text="""
            Name of the account, how it gets represented
            in e.g. a "customers list".

            Example: Moonpeer Inc.
        """)

    legal_name = components.CharField(
        default=None,
        allow_blank=False,
        allow_null=True,
        max_length=80,
        help_text="""
            Legal name of the organisation.
            Only required when it's different from the account name.

            Example: Moon Network Services LLS.
        """)

    address = Address()

    billing_information = BillingInformation(
        default=None,
        allow_null=True,
        required=False)

    external_ref = components.CharField(
        max_length=80,
        default=None,
        allow_null=True,
        allow_blank=False,
        required=False,
        label="External Reference",
        help_text="""
            Reference field, free to use for the API user.
            Example: IX:Service:23042
        """)

    discoverable = components.BooleanField(default=False)


class Account(events.Stateful, AccountBase):
    """Account"""
    id = components.CharField(max_length=80)


class AccountRequest(AccountBase):
    """Account Request"""

class AccountUpdate(AccountBase):
    """Account Update Request"""

#
# Contacts
#


class ContactBase(Ownable, components.Component):
    """
    A contact is a collection of VCARD properties.
    Which fields are required is dependent on the
    role(s) the contact is assigned to.

    For now we only allow name, telephone an d email
    """
    name = components.CharField(
        max_length=128,
        allow_null=True,
        required=False,
        help_text="""
            A name of a person or an organisation
            Example: "Some A. Name"
        """)
    telephone = components.CharField(
        max_length=40,
        allow_null=True,
        required=False,
        help_text="""
            The telephone number in E.164 Phone Number Formatting
            Example: +442071838750
        """)

    email = components.EmailField(
        allow_null=True,
        required=False,
        max_length=80,
        help_text="""
            The email of the legal company entity.

            Example: info@moon-peer.net
        """)


class Contact(ContactBase):
    """Contact"""
    id = components.CharField()

class ContactRequest(ContactBase):
    """A contact creation request"""

class ContactUpdate(ContactBase):
    """A contact update"""


class RoleBase(components.Component):
    """A Role"""
    name = components.CharField(
        max_length=80,
        help_text="""
            The name of the role.

            Example: "noc"
        """)
    required_fields = components.ListField(
        child=components.CharField(max_length=80),
        help_text="""
            A list of required field names.

            Example: ["name", "email"]
        """)


class Role(RoleBase):
    """A role for a contact"""
    id = components.CharField()


class RoleRequest(RoleBase):
    """A new role"""


class RoleUpdate(RoleBase):
    """A role update"""


class RoleAssignmentBase(components.Component):
    """A role can be assigned to a contact"""
    role = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Role"),
        help_text="""
            The `id` of a role the contact is assigned to.

            Example: "role:23"
        """)
    contact = components.PrimaryKeyRelatedField(
        queryset=qs.Any("crm.Contact"),
        help_text="""
            The `id` of a contact the role is assigned to.

            Example: "contact:42b"
        """)


class RoleAssignment(RoleAssignmentBase):
    """A role assignment for a contact"""
    id = components.CharField()


class RoleAssignmentRequest(RoleAssignmentBase):
    """A role assignment request"""


class RoleAssignmentUpdate(RoleAssignmentBase):
    """A role assignemnt update"""


