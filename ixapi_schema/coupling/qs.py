
"""
Coupling - Qs
-------------

Provides a proxy queryset, which will act as a shim
when the schema generator is used as a stand alone application.
"""


from django.db.models import query


class DecoupledQuerySet(query.QuerySet):
    """
    This queryset behaves slightly different,
    when run without the ix-api / django application:
    """
    def __init__(self, model=None, query=None, using=None, hints=None):
        pass

    def __repr__(self):
        return "<DecoupledQuerySet>"


class Any:
    """
    Decouple the queryset of a component from the django application.
    Only resolve the model when django is available.

    The queryset is <app_label>.<model>.objects.all() for Any("app.Model").
    """
    def __init__(self, model_name):
        """
        The queryset is <model>.objects.all(), when
        required and django is available.
        """
        self.model_name = model_name

