
import enum


class IpVersion(enum.Enum):
    IPV4 = 4
    IPV6 = 6


class AddressFamilies(enum.Enum):
    AF_INET = "af_inet"
    AF_INET6 = "af_inet6"
